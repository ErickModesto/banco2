CREATE DATABASE agendamento;

\c agendamento;

CREATE TABLE usuario(
	nome varchar(100) NOT NULL,
	cpf varchar(11) NOT NULL,
	rg int NOT NULL,
	PRIMARY KEY(rg) 
);

CREATE TABLE endereco(
	rg int NOT NULL,
	logradouro varchar(150) NOT NULL,
	numero int NOT NULL,
	cep int NOT NULL,
	CONSTRAINT FK_pessoaEndereco FOREIGN KEY(rg) REFERENCES usuario(rg)
);

CREATE TABLE telefone(
	rg int NOT NULL,
	residencial varchar(11),
	celular varchar(12),
	comercial varchar(12),
	CONSTRAINT FK_pessoaTelefone FOREIGN KEY (rg) REFERENCES usuario (rg)
);

CREATE TABLE paciente(
	rg int NOT NULL,
	planoSaude varchar(20) NOT NULL,
	numeroPlano int NOT NULL,
	CONSTRAINT FK_pessoaPaciente FOREIGN KEY (rg) REFERENCES usuario(rg)
);

CREATE TABLE medico(
	rg int NOT NULL,
	crm int NOT NULL,
	hospital varchar(20) NOT NULL,
	consultorio varchar(20) NOT NULL,
	especialidade varchar(30) NOT NULL,
	PRIMARY KEY (crm),
	CONSTRAINT FK_pessoaMedico FOREIGN KEY (rg) REFERENCES usuario (rg)
);

CREATE TABLE secretaria(
	rg int NOT NULL,
	salario float NOT NULL,
	CONSTRAINT FK_pessoaSecretaria FOREIGN KEY (rg) REFERENCES usuario (rg)
);


CREATE TABLE agenda(
	rg_paciente int NOT NULL,
	crm int NOT NULL,
	dataConsulta timestamp NOT NULL,
	CONSTRAINT FK_AgendaMedico FOREIGN KEY (crm) REFERENCES medico (crm),
	CONSTRAINT FK_AgegendaPaciente FOREIGN KEY (rg_paciente) REFERENCES usuario (rg)
);


CREATE TABLE hospital(
	nome varchar(50) NOT NULL,
	endereco varchar(100) NOT NULL,
	ch int NOT NULL,
	PRIMARY KEY (ch)
);

CREATE TABLE medicoHospital(
	crm int NOT NULL,
	ch int NOT NULL,
	CONSTRAINT FK_medicoHospitalMedico FOREIGN KEY (crm) REFERENCES medico (crm),
	CONSTRAINT FK_medicoHospitalHospital FOREIGN KEY (ch) REFERENCES hospital (ch)
);

