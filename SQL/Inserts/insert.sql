INSERT INTO usuario VALUES
('Erick', '02542375259', '6864570'),
('Suzane', '02215712388', '6768075'),
('Daniel', '02412424024', '6242424'),
('Israel', '02421355590', '6512873'),
('Rodrigo', '02212345678', '6712345'),
('Maria', '12345678909', '1234567');

INSERT INTO endereco VALUES
('6864570', 'Rua Santa Madalena', '360', '67125044'),
('6768075', 'Terra Firme', '500', '66077425'),
('6512873', 'Pedreira', '123', '66083000'),
('6512873', 'Maguari', '50', '67125000'),
('6712345', 'Alcindo Cacela', '35', '66065219'),
('1234567', 'Jurunas', '76', '87444177');

INSERT INTO telefone VALUES
('6864570', '32875603', '984869149', '32736904'),
('6768075', '32662178', '980872989', ' '),
('6512873', '32542456', '984918991', ' '),
('6512873', '32731254', '998036815', ' '),
('6712345', '32436724', '980191392', ' '),
('1234567', '12345678', '987654321', ' ');

INSERT INTO paciente VALUES
('6864570', 'Unimed', '123'),
('6768075', 'Hapvida', '876');

INSERT INTO medico VALUES
('6512873', '12', 'Qualquer', 'Nao sei', 'Ortopedista'),
('6512873', '13', 'MaguariHospital', 'New City Clínica', 'Oftalmologista'),
('6712345', '14', 'TantoFaz', 'Cremação Clinica', 'Pediatra');

INSERT INTO secretaria VALUES
('1234567', '1765');

INSERT INTO hospital VALUES
('CN6', 'Cidade Nova 6', '1'),
('TF', 'Terra Firme', '2');

INSERT INTO medicoHospital VALUES
('12', '2');

INSERT INTO agenda VALUES
('6864570', '12', '2017-10-12 15:30:00'),
('6768075', '13', '2017-10-12 15:30:00');
